Installation
============

- Add the property ENCRYPT_KEY to your system environment, with the value taken from LastPass secure note `JavaDevelopment`. Make sure
  that you restart IntelliJ after changing your system environment.

Introduction
============

This project contains the configuration files for all eriron application. The configuration files are located in the directory
"config" and can be served by a Spring Cloud Config Server (http://cloud.spring.io/spring-cloud-config/single/spring-cloud-config.html).
The Java application in this project is an implementation of a Spring Cloud Config Server, but it is not intended for
production. It is only here to run locally and check the properties and encrypt/decrypt passwords.

Maven
=====

This project uses Maven (https://maven.apache.org/index.html) and Spring Boot (http://projects.spring.io/spring-boot/) to build the software.

Useful maven goals:
* mvn package

    Create the directory "target", which will contain all the created artifacts.
    Compile and test the software, create test reports and create a single jar file.
    <b>Always</b> run this goal before commiting the source to subversion.

* mvn clean

    remove the "target" directory.

* mvn spring-boot:run

    Build the software if necessary and run the application.

* mvn install

    Build the software and store it in the local maven repository.
    In addition it will also create a zip file which is written in the "target" directory,
    as well as a script to deploy the software from the zip onto a server.

POM file
========

The "sortpom-maven-plugin" (https://github.com/Ekryd/sortpom) is used to keep the POM file neat. It will sort the dependencies and properties 
alphabetically and it will keep the formatting the same for each element.

REST interface
==============
In this paragraph an overview is given of all the REST endpoints that this application provides. Use a tool such as
Postman to execute the REST calls. All REST calls require basic authentication (username = ERIRON, password is stored in LastPass 
secure note `JavaDevelopment`).

Get configuration of an application
-----------------------------------
To get the configuration for the application eriron-golf for the PROD environment, make the following REST call:

GET: http://localhost:8888/eriron-golf/PROD

RETURN: all the configuration properties in JSON format

Encrypt password
----------------
Passwords are not allowed to be put in a configuration file in plain text. To encrypt a password  make the following
REST call:

POST: http://localhost:8888/encrypt

BODY: plain-text-password 

RETURN: an encrypted password

Add the encrypted password in the property file prefix with {cipher}. For example {cipher}kh3452lh52kh52kj3562k3b1.

Decrypt password
----------------
To decrypt a password  make the following REST call:

POST: http://localhost:8888/decrypt

BODY: encrypted password

RETURN: plain-text-password
